#Extracts version from app package.json
#!/bin/bash

WORKSPACE=`pwd`
VERSION=`cat ${WORKSPACE}/package.json | grep version | head -1 | cut -d':' -f2 | sed 's/[",]//g'`
echo $VERSION