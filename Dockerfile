FROM node:6 AS builder

MAINTAINER Guido Vilariño <guido@democracyos.org>

RUN npm config set python python2.7

COPY ["package.json", "/usr/src/"]

WORKDIR /usr/src

RUN npm install --quiet

COPY [".", "/usr/src/"]

RUN npm run build -- --minify

ONBUILD COPY ["ext", "ext"]

ONBUILD RUN bin/dos-ext-install --quiet

ONBUILD RUN npm run build -- --minify


#Build production image
FROM node:6-alpine

WORKDIR /usr/src

ENV NODE_ENV=production \
    NODE_PATH=/usr/src

COPY --from=builder /usr/src .

EXPOSE 3000

CMD ["node", "."]